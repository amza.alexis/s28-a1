/* fetch('https://jsonplaceholder.typicode.com/todos')

console.log(fetch('https://jsonplaceholder.typicode.com/todos'))

*/


//Create a fetch request using the GET method that will retrieve all the to do list items from JSON place holder 

fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'GET',})
.then((response) => response.json())

//Using the retrieved data, create an array using the map method to return just the title of every item and print the result in the console.
.then((titleOnly) => {
	const array1 = Object.values(titleOnly);
	const titleArray = array1.map(function(item){
		return item.title
	})
	console.log(titleArray);
});

//Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API

fetch('https://jsonplaceholder.typicode.com/todos/1',{
	method: 'GET',})
.then((response) => response.json())

//Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

.then((num6) => {
	console.log(num6.title)
	console.log(num6.completed)
})

//Create a fetch request using the POST method that will create a to do list item using the JSON placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos',{
	method: 'POST',
	headers: {'Content-Type' : 'application/json'},
	body: JSON.stringify({
		completed: 'true',
		title: 'To-Do',
		userId: 102
	})
})

.then((response) => response.json())
.then((json) => console.log(json))

//Create a fetch request using the PUT method that will update a to do list item using the JSON place holder API.

fetch('https://jsonplaceholder.typicode.com/todos/200',{
	method: 'PUT',
	headers: {'Content-Type' : 'application/json'},
	body: JSON.stringify({
		completed: 'true',
		title: 'Updated To-Do List 8',
		userId: 98
	})
})

.then((response) => response.json())
.then((json) => console.log(json))

//Update a todo list item by changing the data structure to contain the following propperties: Title, Description, Status, Date Completed, User ID

fetch('https://jsonplaceholder.typicode.com/todos/200',{
	method: 'PUT',
	headers: {'Content-Type' : 'application/json'},
	body: JSON.stringify({
		title: 'Updated To-Do List 9',
		description: 'Fill in description',
		completed: 'true',
		dateCompleted: 'October 25, 2021',
		userId: 98
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

//Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/199',{
	method: 'PATCH',
	headers: {'Content-Type' : 'application/json'},
	body: JSON.stringify({
		completed: 'true',
		title: 'Updated To-Do List 10',
		userId: 88
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

//Update a to do list item by changing the status to complete and add a date when the status was changed.

fetch('https://jsonplaceholder.typicode.com/todos/198',{
	method: 'PUT',
	headers: {'Content-Type' : 'application/json'},
	body: JSON.stringify({
		title: 'Updated To-Do List 11',
		description: 'Fill in description',
		completed: 'true',
		dateCompleted: 'October 25, 2021',
		userId: 50
	})
})
.then((response) => response.json())
.then((json) => console.log(json))
